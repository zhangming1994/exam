CREATE TABLE `user` (
`id` int(11) UNSIGNED NOT NULL,
`uid` int(11) UNSIGNED NOT NULL COMMENT '用户编号',
`username` varchar(50) NOT NULL COMMENT '用户名称',
`pwd` varchar(50) NOT NULL COMMENT '用户密码',
`age` int(4) NOT NULL COMMENT '年纪', 
`sex` tinyint(1) NOT NULL COMMENT '性别 1. 男 2. 女',
`birthday` varchar(12) NOT NULL DEFAULT '' COMMENT '用户生日',
`addr` varchar(255) NOT NULL DEFAULT '' COMMENT '用户地址',
`phone` varchar(11) NOT NULL DEFAULT '' COMMENT '用户手机号码',
`email` varchar(20) NOT NULL DEFAULT '' COMMENT '邮箱',
`device_id` varchar(60) NOT NULL DEFAULT '' COMMENT '用户设备号',
`hpic` varchar(100) NOT NULL COMMENT '头像地址(没有上传就准备一些默认的进行随机)',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`uid`),
UNIQUE INDEX `uq_username` (`username` ASC) USING BTREE,
UNIQUE INDEX `uq_phone` (`phone` ASC) USING BTREE,
UNIQUE INDEX `uq_uid` (`uid` ASC) USING BTREE
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='用户表';


CREATE TABLE `paper` (
`id` int(11) UNSIGNED NOT NULL,
`pid` int(11) UNSIGNED NOT NULL COMMENT '试卷编号',
`pname` varchar(50) NOT NULL COMMENT '试卷名称',
`exam_time` int(6) NOT NULL COMMENT '考试时间限制(分钟为单位)',
`exam_rules` varchar(255) NOT NULL COMMENT '考试须知',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`pid`),
UNIQUE INDEX `uq_pname` (`pname` ASC) USING BTREE,
UNIQUE INDEX `uq_pid` (`pid` ASC) USING BTREE
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='试卷表';

CREATE TABLE `paper_detail` (
`id` int(11) UNSIGNED NOT NULL,
`pdid` int(11) UNSIGNED NOT NULL,
`pid` int(11) NOT NULL COMMENT '试卷编号',
`tid` int(11) NOT NULL COMMENT '题目编号',
`order` int(4) NOT NULL COMMENT '题目在试卷里面的编号排序',
`content` varchar(255) NOT NULL COMMENT '题目内容',
`optionA` varchar(255) NOT NULL COMMENT '选项1',
`optionB` varchar(255) NOT NULL COMMENT '选项2',
`optionC` varchar(255) NOT NULL COMMENT '选项3',
`optionD` varchar(255) NOT NULL COMMENT '选项4',
`optionE` varchar(255) NOT NULL COMMENT '选项5',
`optionF` varchar(255) NOT NULL COMMENT '选项6',
`optionG` varchar(255) NOT NULL COMMENT '选项7',
`optionH` varchar(255) NOT NULL COMMENT '选项8',
`optionI` varchar(255) NOT NULL COMMENT '选项9',
`optionJ` varchar(255) NOT NULL COMMENT '选项10',
`right_answer` varchar(10) NOT NULL COMMENT '正确答案',
`value` int(4) NOT NULL COMMENT '分值[处理分值可能不一样的问题]', 
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`),
UNIQUE INDEX `uq_pdid` (`pdid` ASC) USING BTREE
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='试卷详情表';


CREATE TABLE `user_answer` (
`id` int(11) UNSIGNED NOT NULL,
`uid` int(11) NOT NULL COMMENT '用户编号',
`pdid` int(11) NOT NULL COMMENT '试卷详细编号',
`value` int(4) NOT NULL COMMENT '分值',
`user_select` varchar(10) NOT NULL DEFAULT '' COMMENT '选择的答案',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`)
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='用户答案表';


CREATE TABLE `score` (
`id` int(11) UNSIGNED NOT NULL,
`uid` int(11) NOT NULL COMMENT '用户编号',
`pid` int(11) NOT NULL COMMENT '试卷编号',
`score` int(4) NOT NULL COMMENT '成绩',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`)
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='用户成绩表';


CREATE TABLE `paper_classification` (
`id` int(11) UNSIGNED NOT NULL,
`pcid` int(11) UNSIGNED NOT NULL,
`classification`  varchar(255) NOT NULL COMMENT '试题内容分类名称',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`),
UNIQUE INDEX `uq_pcid` (`pcid` ASC) USING BTREE
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='试题分类';


CREATE TABLE `exam_question` (
`id` int(11) UNSIGNED NOT NULL,
`tid` int(11) UNSIGNED NOT NULL,
`content` varchar(255) NOT NULL COMMENT '题目内容',
`pcid` int(11) NOT NULL COMMENT '试题分类ID',
`types` tinyint(1) NOT NULL COMMENT '考题类型 1. 单选 2. 多选',
`level` tinyint(1) NOT NULL COMMENT '试题难易程度(1. 容易 2. 困难 3. 中等)',
`optionA` varchar(255) NOT NULL COMMENT '选项1',
`optionB` varchar(255) NOT NULL COMMENT '选项2',
`optionC` varchar(255) NOT NULL COMMENT '选项3',
`optionD` varchar(255) NOT NULL COMMENT '选项4',
`optionE` varchar(255) NOT NULL COMMENT '选项5',
`optionF` varchar(255) NOT NULL COMMENT '选项6',
`optionG` varchar(255) NOT NULL COMMENT '选项7',
`optionH` varchar(255) NOT NULL COMMENT '选项8',
`optionI` varchar(255) NOT NULL COMMENT '选项9',
`optionJ` varchar(255) NOT NULL COMMENT '选项10',
`right_answer` varchar(10) NOT NULL COMMENT '正确答案',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`tid`),
UNIQUE INDEX `uq_tid` (`tid` ASC) USING BTREE
)Engine=Innodb CHARACTER set utf8mb4 COLLATE utf8mb4_general_ci COMMENT='题库';