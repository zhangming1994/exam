package models

import "time"

type PaperDetail struct {
	ID          int    `gorm:"primary_key"` // 自增主键
	PDID        int    `gorm:"uniqueIndex"` // 试卷详情编号
	PID         int    // 试卷编号
	TID         int    // 题目编号
	Content     string // 题目内容
	OptionA     string // 选项
	OptionB     string //
	OptionC     string //
	OptionD     string //
	OptionE     string //
	OptionF     string //
	OptionG     string //
	OptionH     string //
	OptionI     string //
	OptionJ     string //
	RightAnswer string // 正确答案
	Value       int    // 分值
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
