package models

import "time"

type Paper struct {
	ID        int    `gorm:"primary_key"` // 自增主键
	PID       int    `gorm:"uniqueIndex"` // 试卷编号
	PName     string `gorm:"uniqueIndex"` // 试卷名称
	ExamTime  int64  // 考试时间
	ExamRules string // 考试须知
	CreatedAt time.Time
	UpdatedAt time.Time
}
