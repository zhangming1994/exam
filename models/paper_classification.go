package models

import "time"

type PaperClassification struct {
	ID             int    `gorm:"primary_key"` // 自增主键
	PCID           int    `gorm:"uniqueIndex"`
	Classification string // 试题分类
	CreatedAt      time.Time
	UpdatedAt      time.Time
}
