package models

import "time"

type ExamQuestion struct {
	ID          int    `gorm:"primary_key"` // 自增主键
	TID         int    `gorm:"uniqueIndex"`
	PCID        int    // 试题内容分类ID
	Types       int    // 考题类型 1. 单选 2. 多选
	Level       int    // 试题难易程度 1. 容易 2. 困难 3. 中等
	Content     string // 题目内容
	OptionA     string // 选项
	OptionB     string //
	OptionC     string //
	OptionD     string //
	OptionE     string //
	OptionF     string //
	OptionG     string //
	OptionH     string //
	OptionI     string //
	OptionJ     string //
	RightAnswer string // 正确答案
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
