package models

import "time"

type User struct {
	ID        int    `gorm:"primary_key"` // 自增主键
	Sex       int    // 性别
	Age       int    // 年纪
	Uid       int    `gorm:"uniqueIndex"` // 用户编号
	UserName  string // 用户名称
	Pwd       string // 用户密码
	Birthday  string // 生日
	Addr      string // 地址
	Phone     string // 电话
	Hpic      string // 头像
	Email     string // 手机号码
	DeviceID  string
	CreatedAt time.Time
	UpdatedAt time.Time
}
