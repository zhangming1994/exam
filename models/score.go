package models

import "time"

type Score struct {
	ID        int `gorm:"primary_key"` // 自增主键
	UID       int // 用户ID
	PID       int // 试卷ID
	Score     int // 分值
	CreatedAt time.Time
	UpdatedAt time.Time
}
