package models

import "time"

type UserAnswer struct {
	ID         int    `gorm:"primary_key"` // 自增主键
	UID        int    // 用户ID
	PDID       int    // 试卷ID
	Value      int    // 分值
	UserSelect string // 用户选择的答案
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
