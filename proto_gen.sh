#!/usr/bin/env bash
protoc -I=$1/app/proto/ --go_out=plugins=grpc:./app/services/servers $1/app/proto/servers.proto
