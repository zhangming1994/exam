package server

import (
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"gitlab.com/douwan/exam/app/exam"
	"gitlab.com/douwan/exam/app/home"
	"gitlab.com/douwan/exam/app/paper"
	"gitlab.com/douwan/exam/app/users"
	"gitlab.com/douwan/exam/libs/appleauth"
	"gitlab.com/douwan/exam/utils/logger"
)

func New(db *gorm.DB, sessionStore *sessions.CookieStore, appleAuthClient appleauth.Client) *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.RequestID)
	router.Use(logger.Logger(logger.Option{
		ServiceName: "ApiBasic",
		FormattedTime: func(t time.Time) string {
			return t.In(time.FixedZone("local", 8*60*60)).Format("2006-01-02/15:04:05")
		},
		Keys: []string{"service", "ip", "user", "date", "path", "params", "duration"},
	}))
	router.Use(middleware.Recoverer)
	router.Use(middleware.URLFormat)
	router.Use(Authentication(sessionStore))

	usersHandlers := users.NewHandler(db, appleAuthClient, sessionStore)
	router.Post("/users/apple/auth", usersHandlers.Auth)
	router.Get("/api/v1/my", usersHandlers.GetUser)
	router.Put("/api/v1/my", usersHandlers.UpdateUser)
	router.Post("/api/v1/register", usersHandlers.Register)
	router.Post("/api/v1/login", usersHandlers.Login)

	homeHandlers := home.NewHandler(db, sessionStore)
	router.Get("/", homeHandlers.Home)
	router.Get("/api/v1/socre", homeHandlers.GetExamScore)
	router.Get("/api/v1/examdetail", homeHandlers.GetExamScoreDetail)

	paperHandlers := paper.NewHandler(db, sessionStore)
	router.Post("/api/v1/paper", paperHandlers.HandOverPaper)
	router.Get("/api/v1/paper", paperHandlers.GetPaper)

	examHandlers := exam.NewHandler(db, sessionStore)
	router.Post("/api/v1/question", examHandlers.NewQuestion)
	router.Get("/api/v1/questions", examHandlers.QuestionList)
	router.Put("/api/v1/question", examHandlers.UpdateQuestion)
	router.Delete("/api/v1/question", examHandlers.DelQuestion)
	return router
}

func Authentication(sessionStore *sessions.CookieStore) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			session, err := sessionStore.Get(r, "exam")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			authStr := r.Header.Get("Authorization")
			accessToken := strings.TrimPrefix(authStr, "Token token=")
			if accessToken != "" {
				session.Values["token"] = accessToken
				session.Save(r, w)
			}

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
