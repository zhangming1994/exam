package config

import (
	"github.com/jinzhu/configor"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type AppConfig struct {
	Port              string `default:"9000"`
	DB                string
	AppleAuthTeamID   string
	AppleAuthBundleID string
	AppleAuthKey      string
	AppleAuthKeyPath  string
}

var _AppConfig *AppConfig

func MustGetAppConfig() AppConfig {
	if _AppConfig != nil {
		return *_AppConfig
	}
	appConfig := &AppConfig{}
	err := configor.New(&configor.Config{ENVPrefix: "APP"}).Load(appConfig)
	if err != nil {
		panic(err)
	}
	_AppConfig = appConfig
	return *_AppConfig
}

func MustGetDB() *gorm.DB {
	c := MustGetAppConfig()
	DB, err := gorm.Open("mysql", c.DB)
	if err != nil {
		panic(err)
	}
	//if os.Getenv("DEBUG") != "" {
	DB.LogMode(true)
	//}
	return DB
}

/*
func InitSentryConfig() {
	c := MustGetAppConfig()
	raven.SetDSN(c.SentryAddress)
}*/
