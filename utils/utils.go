package utils

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis"
)

func FormatTime(t *time.Time) string {
	if t == nil {
		return ""
	}
	return t.Format("2006-01-02 15:04:05")
}

// Round 浮点数转字符串 (避免科学计数法)
func Round(f float64, size ...int) string {
	fa := strings.Split(fmt.Sprintf("%.8f", f), ".")
	if len(size) > 0 && size[0] == 2 {
		fa = strings.Split(fmt.Sprintf("%.2f", f), ".")
	}
	intStr := fa[0]
	digitalStr := strings.TrimRight(fa[1], "0")
	in := 0
	if intStr != "0" {
		in = len(intStr)
	}
	dn := len(digitalStr)
	return strconv.FormatFloat(f, 'g', in+dn, 64)
}

func RoundFloat(f float64, n int) float64 {
	pow10_n := math.Pow10(n) //对10取n次幂
	//然后获取一个小数位比需要的大一位的浮点数，加上传递进来的浮点数，然后乘以10的n次幂，得到一个小数点后一位的
	//浮点数，然后math.trunc取整,除以10的n次幂，去掉加权小数
	return math.Trunc((f+0.5/pow10_n)*pow10_n) / pow10_n
}

type WebSession struct {
	User SessionUser `json:"user"`
}

type SessionUser struct {
	AccessToken string `json:"access_token"`
}

func FetchAccessToken(r *redis.Client, req *http.Request) string {
	sessionID := getSessionIdFromCookie(req)
	sessionString, err := r.Get(sessionID).Result()
	if err != nil {
		return ""
	}
	var session WebSession
	err = json.Unmarshal([]byte(sessionString), &session)
	if err != nil {
		return ""
	}
	return strings.Split(session.User.AccessToken, ":")[0]
}

func getSessionIdFromCookie(r *http.Request) string {
	cookie, _ := r.Cookie("connect.sid")
	connectSid := cookie.Value
	splitSid := strings.Split(connectSid, ".")
	sid := splitSid[0]
	sid, _ = url.QueryUnescape(sid)
	sid = strings.Split(sid, ":")[1]
	return fmt.Sprintf("sess:%v", sid)
}

func GenerateUuid(id int64) string {
	numbers := id * 7777 % 1000
	productNumber := fmt.Sprintf("%04d", numbers)
	partTime := time.Now().Format("200601021504")
	return fmt.Sprintf("%s%s", partTime, productNumber)
}

func FormattedVolumn(volume float64) string {
	if volume < 100 {
		return Round(volume, 8)
	} else if volume < 1000000 {
		return Round(volume, 4)
	} else if volume < 100000000 {
		return Round(volume, 2)
	} else {
		return fmt.Sprintf("%v", int(volume))
	}
}

//日期的时区转换
func LocalYesterdayDate(today time.Time, offset int) string {
	return today.In(time.FixedZone("local", offset*60*60)).AddDate(0, 0, -1).Format("2006-01-02")
}

func LocalHour(today time.Time, offset int) string {
	return today.In(time.FixedZone("local", offset*60*60)).Format("15")
}

func BeginningOfDayInUTC(date string, offset int) time.Time {
	now, _ := time.ParseInLocation("2006-01-02 15:04:05", date+" 00:00:01", time.FixedZone("local", offset*60*60))
	return now.UTC()
}

func EndOfDayInUTC(date string, offset int) time.Time {
	now, _ := time.ParseInLocation("2006-01-02 15:04:05", date+" 23:59:59", time.FixedZone("local", offset*60*60))
	return now.UTC()
}
