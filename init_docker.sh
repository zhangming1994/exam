#!/usr/bin/env bash
docker-compose -f docker-compose.yml down
docker-compose -f docker-compose.yml up -d

sleep 20

docker cp config/db/scheme.sql exam_mysql_1:/
docker exec exam_mysql_1 sh -c "mysql -uroot -p123 exam_dev < /scheme.sql"

docker cp config/db/scheme.sql exam_mysql_test_1:/
docker exec exam_mysql_test_1 sh -c "mysql -uroot -p123 exam_test < /scheme.sql"
