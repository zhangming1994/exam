package exam

import (
	"net/http"
	"time"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"gitlab.com/douwan/exam/app/entries"
	"gitlab.com/douwan/exam/app/helpers"
	"gitlab.com/douwan/exam/models"
)

type Handler struct {
	DB           *gorm.DB
	SessionStore *sessions.CookieStore
}

func NewHandler(db *gorm.DB, sessionStore *sessions.CookieStore) Handler {
	return Handler{DB: db, SessionStore: sessionStore}
}

//QuestionList  题目列表
func (h Handler) QuestionList(w http.ResponseWriter, r *http.Request) {
	examQuestionList := make([]entries.ExamQuestions, 0)
	h.DB.Table("exam_question").Find(&examQuestionList)
	helpers.RenderSuccessJSON(w, examQuestionList)
}

// NewQuestion 增加题目
func (h Handler) NewQuestion(w http.ResponseWriter, r *http.Request) {
	content := r.FormValue("content")
	if content == "" {
		helpers.RenderFailureJSON(w, 500, "题目内容为空")
		return
	}
	pcid := helpers.GetParamsInt(r, "pcid")
	if pcid == 0 {
		helpers.RenderFailureJSON(w, 500, "试题分类错误")
		return
	}
	types := helpers.GetParamsInt(r, "types")
	if types != 1 && types != 2 {
		helpers.RenderFailureJSON(w, 500, "考题类型错误")
		return
	}
	// Check is omitted at these locations
	level := helpers.GetParamsInt(r, "level")
	optionA := r.FormValue("optionA")
	optionB := r.FormValue("optionB")
	optionC := r.FormValue("optionC")
	optionD := r.FormValue("optionD")
	optionE := r.FormValue("optionE")
	optionF := r.FormValue("optionF")
	optionG := r.FormValue("optionG")
	optionH := r.FormValue("optionH")
	optionI := r.FormValue("optionI")
	optionJ := r.FormValue("optionJ")
	rightAnswer := r.FormValue("right")
	examQuestion := new(models.ExamQuestion)
	examQuestion.TID = int(time.Now().Unix()) // The generation algorithm is not determined
	examQuestion.Content = content
	examQuestion.PCID = pcid
	examQuestion.Types = types
	examQuestion.Level = level
	examQuestion.OptionA = optionA
	examQuestion.OptionB = optionB
	examQuestion.OptionC = optionC
	examQuestion.OptionD = optionD
	examQuestion.OptionE = optionE
	examQuestion.OptionF = optionF
	examQuestion.OptionG = optionG
	examQuestion.OptionH = optionH
	examQuestion.OptionI = optionI
	examQuestion.OptionJ = optionJ
	examQuestion.RightAnswer = rightAnswer
	h.DB.Table("exam_question").Create(&examQuestion)
}

//UpdateQuestion  修改题目
func (h Handler) UpdateQuestion(w http.ResponseWriter, r *http.Request) {
	content := r.FormValue("content")
	if content == "" {
		helpers.RenderFailureJSON(w, 500, "题目内容为空")
		return
	}
	pcid := helpers.GetParamsInt(r, "pcid")
	if pcid == 0 {
		helpers.RenderFailureJSON(w, 500, "试题分类错误")
		return
	}
	types := helpers.GetParamsInt(r, "types")
	if types != 1 && types != 2 {
		helpers.RenderFailureJSON(w, 500, "考题类型错误")
		return
	}
	// Check is omitted at these locations
	level := helpers.GetParamsInt(r, "level")
	optionA := r.FormValue("optionA")
	optionB := r.FormValue("optionB")
	optionC := r.FormValue("optionC")
	optionD := r.FormValue("optionD")
	optionE := r.FormValue("optionE")
	optionF := r.FormValue("optionF")
	optionG := r.FormValue("optionG")
	optionH := r.FormValue("optionH")
	optionI := r.FormValue("optionI")
	optionJ := r.FormValue("optionJ")
	rightAnswer := r.FormValue("right")
	examQuestion := new(models.ExamQuestion)
	examQuestion.TID = int(time.Now().Unix()) // The generation algorithm is not determined
	examQuestion.Content = content
	examQuestion.PCID = pcid
	examQuestion.Types = types
	examQuestion.Level = level
	examQuestion.OptionA = optionA
	examQuestion.OptionB = optionB
	examQuestion.OptionC = optionC
	examQuestion.OptionD = optionD
	examQuestion.OptionE = optionE
	examQuestion.OptionF = optionF
	examQuestion.OptionG = optionG
	examQuestion.OptionH = optionH
	examQuestion.OptionI = optionI
	examQuestion.OptionJ = optionJ
	examQuestion.RightAnswer = rightAnswer
	h.DB.Table("exam_question").Update(&examQuestion)
}

//DelQuestion  删除题目
func (h Handler) DelQuestion(w http.ResponseWriter, r *http.Request) {
	id := helpers.GetParamsInt(r, "id")
	examQuestion := new(models.ExamQuestion)
	examQuestion.ID = id
	result := h.DB.Table("exam_question").Delete(&examQuestion)
	if result.RowsAffected == 1 {
		helpers.RenderSuccessJSON(w, entries.Success{Success: true})
	}
}
