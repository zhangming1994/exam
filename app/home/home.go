package home

import (
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"gitlab.com/douwan/exam/app/entries"
	"gitlab.com/douwan/exam/app/helpers"
)

type Handler struct {
	DB           *gorm.DB
	SessionStore *sessions.CookieStore
}

func NewHandler(db *gorm.DB, sessionStore *sessions.CookieStore) Handler {
	return Handler{DB: db, SessionStore: sessionStore}
}

func (h Handler) Home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`HelloWorld!`))
}

//GetExamScore 获取用户所有试卷考试分数
func (h Handler) GetExamScore(w http.ResponseWriter, r *http.Request) {
	userid := helpers.GetParamsInt(r, "uid")
	allpaperSocre := make([]entries.ScoreList, 0)
	h.DB.Table("score").Where("uid = ?", userid).Select("pid", "score").Find(&allpaperSocre)
	if len(allpaperSocre) == 0 {
		helpers.RenderSuccessJSON(w, nil)
	} else {
		helpers.RenderSuccessJSON(w, entries.Success{Success: true, Data: allpaperSocre})
	}
}

//GetExamScoreDetail 获取某一份试卷考试详情
func (h Handler) GetExamScoreDetail(w http.ResponseWriter, r *http.Request) {
	userid := helpers.GetParamsInt(r, "uid")
	pid := helpers.GetParamsInt(r, "pid")
	// 获取试卷详细内容
	paperinfos := make([]entries.PaperInfo, 0)
	h.DB.Table("paper_detail").Where("pid = ?", pid).Select("pid", "pdid", "tid", "order", "content", "optionA", "optionB", "optionC", "optionD", "optionE", "optionF", "optionG", "optionH", "optionI", "optionJ").Find(&paperinfos)

	// 获取用户对应的答案
	useranswer := make([]entries.UserAnswer, 0)
	h.DB.Table("user_answer").Where("uid = ?", userid).Find(&useranswer)

	for _, v := range paperinfos {
		for _, answer := range useranswer {
			if v.PDID == answer.PDID {
				v.UserAnswer = answer.UserSelect
			}
		}
	}
	helpers.RenderSuccessJSON(w, entries.Success{Success: true, Data: paperinfos})
}
