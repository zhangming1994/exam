package users

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"gitlab.com/douwan/exam/app/entries"
	"gitlab.com/douwan/exam/app/helpers"
	"gitlab.com/douwan/exam/libs/appleauth"
	"gitlab.com/douwan/exam/models"
)

type Handler struct {
	DB              *gorm.DB
	AppleAuthClient appleauth.Client
	SessionStore    *sessions.CookieStore
}

func NewHandler(db *gorm.DB, appleAuthClient appleauth.Client, sessionStore *sessions.CookieStore) Handler {
	return Handler{DB: db, AppleAuthClient: appleAuthClient, SessionStore: sessionStore}
}

func (h Handler) Auth(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("token")
	if token == "" {
		helpers.RenderFailureJSON(w, 500, "凭证不能为空")
		return
	}

	deviceID := r.FormValue("device_id")
	if deviceID == "" {
		helpers.RenderFailureJSON(w, 500, "设备ID不能为空")
		return
	}

	u, err := h.AppleAuthClient.Validate(token)
	if err != nil {
		helpers.RenderFailureJSON(w, 500, "无效的凭证")
		return
	}

	user := models.User{}
	id, _ := strconv.ParseInt(u.ID, 10, 64) // TODO
	h.DB.Where("uid = ?", u.ID).First(&user)
	if h.DB.NewRecord(user) {
		user.Uid = int(id)
		user.Email = u.Email
		if err := h.DB.Save(&user).Error; err != nil {
			helpers.RenderFailureJSON(w, 500, "保存用户失败")
			return
		}
	}
	helpers.RenderSuccessJSON(w, entries.AuthResp{
		DeviceID: user.DeviceID,
	})
}

func (h Handler) Login(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	if username == "" {
		helpers.RenderFailureJSON(w, 400, "用户名称不能为空")
		return
	}

	password := r.FormValue("password")
	if password == "" {
		helpers.RenderFailureJSON(w, 400, "密码不能为空")
		return
	}
	user := &models.User{}
	h.DB.Where("username = ? AND pwd = ?", username, password).First(user)

	if h.DB.NewRecord(user) {
		helpers.RenderFailureJSON(w, 400, "找不到用户")
		return
	} else {
		// TODO set session and back other need data
		helpers.RenderSuccessJSON(w, "ok")
	}
}

// Register
func (h Handler) Register(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	if username == "" {
		helpers.RenderFailureJSON(w, 400, "用户名称不能为空")
		return
	}

	deviceID := r.FormValue("device_id")
	if deviceID == "" {
		helpers.RenderFailureJSON(w, 400, "设备ID不能为空")
		return
	}

	password := r.FormValue("password")
	if password == "" {
		helpers.RenderFailureJSON(w, 400, "密码不能为空")
		return
	}
	age := helpers.GetParamsInt(r, "age")
	sex := helpers.GetParamsInt(r, "sex")
	uid := time.Now().Unix()
	user := &models.User{
		Sex:      sex,
		Age:      age,
		Uid:      int(uid),
		Pwd:      password, // TODO  Encryption is variable
		UserName: username,
		DeviceID: deviceID,
		Hpic:     "laskdjfhkj", // hpic just write it for the moment
		// other fields
	}
	if err := h.DB.Create(user).Error; err != nil {
		helpers.RenderSuccessJSON(w, entries.Success{Success: false, Data: err.Error()})
	} else {
		helpers.RenderSuccessJSON(w, entries.Success{Success: true})
	}
}

func (h Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	currentUser, err := helpers.GetCurrentUser(r, h.DB, h.SessionStore)
	if err != nil {
		helpers.RenderFailureJSON(w, 500, err.Error())
		return
	}

	user := &models.User{}
	h.DB.Where("uid = ?", currentUser.Uid).First(user)
	if h.DB.NewRecord(user) {
		helpers.RenderFailureJSON(w, 500, "找不到用户")
		return
	}

	helpers.RenderSuccessJSON(w, entries.UserResp{
		DeviceID: user.DeviceID,
	})
}

func (h Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	currentUser, err := helpers.GetCurrentUser(r, h.DB, h.SessionStore)
	if err != nil {
		helpers.RenderFailureJSON(w, 500, err.Error())
		return
	}

	user := &models.User{}
	h.DB.Where("device_id = ?", currentUser.DeviceID).First(user)

	if h.DB.NewRecord(user) {
		helpers.RenderFailureJSON(w, 500, "找不到用户")
		return
	}

	h.DB.Save(&user)
	helpers.RenderSuccessJSON(w, entries.UserResp{
		DeviceID: user.DeviceID,
	})
}
