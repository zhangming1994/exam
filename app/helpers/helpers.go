package helpers

import (
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/sessions"
	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"

	"gitlab.com/douwan/exam/app/entries"
	"gitlab.com/douwan/exam/models"
)

func RenderFailureJSON(w http.ResponseWriter, code int, message string) {
	result, _ := json.Marshal(entries.Error{
		Success: false,
		Message: message,
	})
	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

func RenderSuccessJSON(w http.ResponseWriter, data interface{}) {
	result, _ := json.Marshal(entries.Success{
		Success: true,
		Data:    data,
	})
	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

func MulDecimal(a string, b string) (string, error) {
	f1, _, err := big.ParseFloat(a, 10, 50, big.ToNearestEven)
	if err != nil {
		return "", err
	}

	f2, _, err := big.ParseFloat(b, 10, 50, big.ToNearestEven)
	if err != nil {
		return "", err
	}

	return f1.Mul(f1, f2).String(), nil
}

func GetCurrentUser(r *http.Request, db *gorm.DB, sessionStore *sessions.CookieStore) (*models.User, error) {
	session, err := sessionStore.Get(r, "exam")
	if err != nil {
		return nil, err
	}
	deviceID, ok := session.Values["token"].(string)
	if !ok {
		return nil, fmt.Errorf("DeviceID不能为空")
	}
	user := &models.User{}
	db.Where("device_id = ?", deviceID).First(user)
	if db.NewRecord(user) {
		user.DeviceID = deviceID
		db.Save(user)
	}
	return user, nil
}

func GetParams(r *http.Request, params string) string {
	url := r.FormValue(strcase.ToCamel(params))
	if url == "" {
		url = r.FormValue(strings.ToLower(strcase.ToScreamingSnake(params)))
	}
	return url
}

func GetParamsInt(r *http.Request, params string) int {
	value := GetParams(r, params)
	i, _ := strconv.Atoi(value)
	return i
}

func GetParamsBool(r *http.Request, params string) bool {
	value := GetParams(r, params)
	if value == "1" || value == "true" {
		return true
	}
	if value == "0" || value == "false" {
		return false
	}
	return false
}
