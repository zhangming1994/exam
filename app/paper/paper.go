package paper

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"gitlab.com/douwan/exam/app/entries"
	"gitlab.com/douwan/exam/app/helpers"
	"gitlab.com/douwan/exam/models"
)

type Handler struct {
	DB           *gorm.DB
	SessionStore *sessions.CookieStore
}

func NewHandler(db *gorm.DB, sessionStore *sessions.CookieStore) Handler {
	return Handler{DB: db, SessionStore: sessionStore}
}

//GetPaper 用户考试前获取试卷
func (h Handler) GetPaper(w http.ResponseWriter, r *http.Request) {
	pid := helpers.GetParamsInt(r, "pid")
	paperTitle := new(entries.PaperTitle)
	h.DB.Table("paper").Where("pid = ?", pid).First(paperTitle)

	paperInfos := make([]entries.PaperDetail, 0)
	h.DB.Table("paper_detail").Where("pid = ?", pid).Find(&paperInfos)
	helpers.RenderSuccessJSON(w, entries.GetPaper{
		PaperTitle: *paperTitle,
		PaperInfos: paperInfos,
	})
}

//HandOverPaper 提交试卷
func (h Handler) HandOverPaper(w http.ResponseWriter, r *http.Request) {
	data := r.FormValue("data")
	var scoreover entries.SocrePaper
	if err := json.Unmarshal([]byte(data), &scoreover); err != nil {
		helpers.RenderFailureJSON(w, 500, err.Error())
		return
	}
	score := new(models.Score)
	score.UID = scoreover.UID
	score.PID = scoreover.PID
	score.Score = scoreover.TotalScore
	h.DB.Table("score").Create(&score)

	list := make([]models.UserAnswer, len(scoreover.ScoreDetail))
	for i := 0; i < len(scoreover.ScoreDetail); i++ {
		list[i].UID = scoreover.UID
		list[i].PDID = scoreover.ScoreDetail[i].PDID
		list[i].Value = scoreover.ScoreDetail[i].Score
		list[i].UserSelect = scoreover.ScoreDetail[i].Answer
	}
	h.DB.Table("user_answer").Create(list)
	helpers.RenderSuccessJSON(w, entries.Success{Success: true})
}
