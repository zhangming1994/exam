package entries

import "time"

type ScoreList struct {
	PID   int `json:"pid"`
	Score int `json:"score"`
}

type PaperInfo struct {
	PID        int    `json:"pid"`
	PDID       int    `json:"pdid"`
	TID        int    `json:"tid"`
	Order      int    `json:"order"`
	Content    string `json:"content"`
	OptionA    string `json:"optionA"`
	OptionB    string `json:"optionB"`
	OptionC    string `json:"optionC"`
	OptionD    string `json:"optionD"`
	OptionE    string `json:"optionE"`
	OptionF    string `json:"optionF"`
	OptionG    string `json:"optionG"`
	OptionH    string `json:"optionH"`
	OptionI    string `json:"optionI"`
	OptionJ    string `json:"optionJ"`
	UserAnswer string `json:"user_answer" gorm:"-"`
}

type UserAnswer struct {
	PDID       int    `json:"pdid"`
	UserSelect string `json:"user_select"`
}

type ExamQuestions struct {
	ID         int       `json:"id"`
	TID        int       `json:"tid"`
	Content    string    `json:"content"`
	Types      string    `json:"types"`
	Level      int       `json:"level"`
	CreateTime time.Time `json:"create_time"`
}

type SocrePaper struct {
	UID         int `json:"uid"`
	PID         int `json:"pid"`
	TotalScore  int `json:"total_score"`
	ScoreDetail []struct {
		PDID   int    `json:"pdid"`
		Answer string `json:"answer"`
		Score  int    `json:"score"`
	} `json:"score_detail"`
}
