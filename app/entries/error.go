package entries

type Error struct {
	Success bool        `json:"success"`
	Message interface{} `json:"message"`
}
