package entries

type AuthResp struct {
	DeviceID string `json:"device_id"`
}

type UserResp struct {
	DeviceID string `json:"device_id"`
	VipDays  int    `json:"vip_days"`
}

type UserRegister struct {
	Status bool   `json:"status"`
	Msg    string `json:"msg"`
}
