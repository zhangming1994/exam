package entries

type PaperTitle struct {
	PName     string `json:"pname"`
	PID       int    `json:"pid"`
	ExamTime  int    `json:"exam_time"`
	ExamRules string `json:"exam_rules"`
}

type PaperDetail struct {
	PDID    int    `json:"pdid"`
	TID     int    `json:"tid"`
	Order   int    `json:"order"`
	Value   int    `json:"value"`
	Content string `json:"content"`
	OptionA string `json:"optionA"`
	OptionB string `json:"optionB"`
	OptionC string `json:"optionC"`
	OptionD string `json:"optionD"`
	OptionE string `json:"optionE"`
	OptionF string `json:"optionF"`
	OptionG string `json:"optionG"`
	OptionH string `json:"optionH"`
	OptionI string `json:"optionI"`
	OptionJ string `json:"optionJ"`
}

type GetPaper struct {
	PaperTitle PaperTitle    `json:"paper_title"`
	PaperInfos []PaperDetail `json:"paper_detail"`
}
