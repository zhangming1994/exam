package appleauth

import (
	"context"
	"io/ioutil"

	"github.com/Timothylock/go-signin-with-apple/apple"
	"gitlab.com/douwan/exam/config"
)

type User struct {
	ID    string
	Email string
}

type Client interface {
	Validate(token string) (*User, error)
}

type AppleClient struct {
	Client *apple.Client
}

func New() (serv *AppleClient, err error) {
	client := AppleClient{}
	client.Client = apple.New()
	return &client, nil
}

func (client AppleClient) Validate(token string) (*User, error) {
	clientID, secret, err := client.getConfig()
	if err != nil {
		return nil, err
	}

	vReq := apple.AppValidationTokenRequest{
		ClientID:     clientID,
		ClientSecret: secret,
		Code:         token,
	}

	var resp apple.ValidationResponse
	client.Client.VerifyAppToken(context.Background(), vReq, &resp)
	claim, err := apple.GetClaims(resp.IDToken)
	if err != nil {
		return nil, err
	}

	//emailVerified := (*claim)["email_verified"]
	//isPrivateEmail := (*claim)["is_private_email"]
	return &User{ID: (*claim)["sub"].(string), Email: (*claim)["email"].(string)}, nil
}

func (client AppleClient) getConfig() (string, string, error) {
	cfg := config.MustGetAppConfig()
	teamID := cfg.AppleAuthTeamID
	clientID := cfg.AppleAuthBundleID
	keyID := cfg.AppleAuthKey
	data, err := ioutil.ReadFile(cfg.AppleAuthKeyPath)
	if err != nil {
		return "", "", err
	}
	secret, err := apple.GenerateClientSecret(string(data), teamID, clientID, keyID)
	return clientID, secret, err
}
