package test

import (
	"net/url"
	"strings"
	"testing"

	"github.com/fatih/color"
)

type UserAuthTestCase struct {
	Token            string
	DeviceID         string
	ExpectedError    string
	ExpectedDeviceID string
	ExpectedUsers    string
}

func TestUsersAuth(t *testing.T) {
	setup()
	RunSQL(DB, `TRUNCATE users`)

	testCases := []UserAuthTestCase{
		// 1. Token为空
		{Token: "", ExpectedError: "凭证不能为空"},
		// 2. DeviceID为空
		{Token: "token1", DeviceID: "", ExpectedError: "设备ID不能为空"},
		// 3. Token是无效的
		{Token: "Invalid", DeviceID: "device1", ExpectedError: "无效的凭证"},
		// 4. 成功返回
		{Token: "token1", DeviceID: "device1", ExpectedDeviceID: "device1", ExpectedUsers: "1,123,a@gmail.com,device1"},
		// 5. 还是返回第一次绑定的Device ID
		{Token: "token1", DeviceID: "device2", ExpectedDeviceID: "device1", ExpectedUsers: "1,123,a@gmail.com,device1"},
		// 6. 另一个用户
		{Token: "token2", DeviceID: "device3", ExpectedDeviceID: "device3", ExpectedUsers: "1,123,a@gmail.com,device1; 2,456,b@gmail.com,device3"},
	}

	for i, testCase := range testCases {
		body := Post("/users/apple/auth", url.Values{"token": {testCase.Token}, "device_id": {testCase.DeviceID}})
		if testCase.ExpectedError == "" && strings.Contains(body, "error") {
			t.Errorf(color.RedString("TestUsersAuth #%v: Expected don't get error but got %v", i+1, body))
		}
		if testCase.ExpectedError != "" && !strings.Contains(body, testCase.ExpectedError) {
			t.Errorf(color.RedString("TestUsersAuth #%v: Expected get error '%v' but got %v", i+1, testCase.ExpectedError, body))
		}
		if !strings.Contains(body, testCase.ExpectedDeviceID) {
			t.Errorf(color.RedString("TestUsersAuth #%v: Expected get device_id %v but got %v", i+1, testCase.DeviceID, body))
		}
		users := GetRecords(DB, "users", "id, uid, email, device_id")
		if testCase.ExpectedUsers != "" && testCase.ExpectedUsers != users {
			t.Errorf(color.RedString("TestUsersAuth #%v: Expected users data is %v but got %v", i+1, testCase.ExpectedUsers, users))
		}
		color.Green("TestUsersAuth #%v: Success", i+1)
	}
}
