package test

import (
	"fmt"
	"io/ioutil"
	"net/http/httptest"
	"net/url"
	"strings"
	"time"

	httpclient "github.com/ddliu/go-httpclient"
	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"gitlab.com/douwan/exam/config"
	"gitlab.com/douwan/exam/libs/appleauth"
	"gitlab.com/douwan/exam/models"
	"gitlab.com/douwan/exam/server"
)

var (
	DB           *gorm.DB
	Server       *httptest.Server
	SessionStore *sessions.CookieStore
)

type Record struct {
	Value string
}

type MockAppleAuthClient struct {
}

func (client MockAppleAuthClient) Validate(token string) (*appleauth.User, error) {
	if token == "Invalid" {
		return nil, fmt.Errorf("Invalid")
	}
	if token == "token1" {
		return &appleauth.User{ID: "123", Email: "a@gmail.com"}, nil
	}
	if token == "token2" {
		return &appleauth.User{ID: "456", Email: "b@gmail.com"}, nil
	}
	return nil, fmt.Errorf("No set")
}

func setup() {
	c := config.MustGetAppConfig()
	if !strings.Contains(c.DB, "_test") {
		panic("It isn't test db.")
	}
	var err error
	DB, err = gorm.Open("mysql", c.DB)
	//DB.LogMode(true)
	if err != nil {
		panic(err)
	}
	DB.DropTableIfExists(&models.User{})
	DB.AutoMigrate(&models.User{})
	createData()

	SessionStore = sessions.NewCookieStore([]byte("something-very-secret"))
	router := server.New(DB, SessionStore, MockAppleAuthClient{})
	Server = httptest.NewServer(router)
}

func createData() {
	RunSQL(DB, `
        INSERT INTO users (id, device_id, email) VALUES (1, 'DB-1', 'device1@a.cn');
        INSERT INTO users (id, device_id, email) VALUES (2, 'DB-2', 'device2@a.cn');
	`)
}

func Get(url string) string {
	client := httpclient.NewHttpClient()
	resp, _ := client.Get(Server.URL + url)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func SignedGet(token string, url string) string {
	client := httpclient.NewHttpClient()
	client.Headers = make(map[string]string)
	client.Headers["Authorization"] = fmt.Sprintf("Token token=%v", token)
	resp, _ := client.Get(Server.URL + url)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func Put(url string, values url.Values) string {
	client := httpclient.NewHttpClient()
	client.Headers = make(map[string]string)
	client.Headers["Content-Type"] = "application/x-www-form-urlencoded"
	params := strings.NewReader(values.Encode())
	resp, _ := client.Put(Server.URL+url, params)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func SignedPut(token string, url string, values url.Values) string {
	client := httpclient.NewHttpClient()
	client.Headers = make(map[string]string)
	client.Headers["Content-Type"] = "application/x-www-form-urlencoded"
	client.Headers["Authorization"] = fmt.Sprintf("Token token=%v", token)
	params := strings.NewReader(values.Encode())
	resp, _ := client.Put(Server.URL+url, params)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func SignedDelete(token string, url string) string {
	client := httpclient.NewHttpClient()
	client.Headers = make(map[string]string)
	client.Headers["Content-Type"] = "application/x-www-form-urlencoded"
	client.Headers["Authorization"] = fmt.Sprintf("Token token=%v", token)
	resp, _ := client.Delete(Server.URL + url)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func Post(url string, values url.Values) string {
	client := httpclient.NewHttpClient()
	client.Headers = make(map[string]string)
	params := make(map[string]string)
	for k, value := range values {
		params[k] = value[0]
	}
	resp, _ := client.Post(Server.URL+url, params)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func SignedPost(token string, url string, values url.Values) string {
	client := httpclient.NewHttpClient()
	client.Headers = make(map[string]string)
	client.Headers["Authorization"] = fmt.Sprintf("Token token=%v", token)
	params := make(map[string]string)
	for k, value := range values {
		params[k] = value[0]
	}
	resp, _ := client.Post(Server.URL+url, params)
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}

func GetRecords(db *gorm.DB, tableName string, columns string, extra ...string) string {
	var (
		extraSQL = ""
		results  = []string{}
		records  = []Record{}
	)
	if len(extra) > 0 {
		extraSQL = extra[0]
	}
	db.Raw(fmt.Sprintf(`SELECT CONCAT_WS(',', %v) AS value FROM %v %v`, columns, tableName, extraSQL)).Scan(&records)
	for _, record := range records {
		results = append(results, record.Value)
	}
	return strings.Join(results, "; ")
}

type DeletedAt struct {
	DeletedAt time.Time
}

func RunSQL(db *gorm.DB, sqls string) {
	for _, sql := range strings.Split(sqls, "\n") {
		if strings.TrimSpace(sql) != "" {
			db.Exec(strings.TrimSpace(sql))
		}
	}
}
