// http.go
package main

import (
	"log"
	"net/http"

	"gitlab.com/douwan/exam/config"
	"gitlab.com/douwan/exam/libs/appleauth"
	"gitlab.com/douwan/exam/models"
	"gitlab.com/douwan/exam/server"

	"github.com/gorilla/sessions"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	c := config.MustGetAppConfig()

	db := config.MustGetDB()
	db.AutoMigrate(&models.User{}, &models.ExamQuestion{}, &models.Paper{}, &models.PaperClassification{},
		&models.PaperDetail{}, &models.Score{}, &models.UserAnswer{})
	defer db.Close()

	appleAuthClient, err := appleauth.New()
	if err != nil {
		panic(err)
	}

	sessionStore := sessions.NewCookieStore([]byte("308f71fd3c4913ed65f25533f22dbc10"))
	router := server.New(db, sessionStore, appleAuthClient)

	log.Printf("========== Visit http://localhost%v ==========\n", c.Port)
	log.Fatal(http.ListenAndServe(c.Port, router))
}
