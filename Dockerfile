##
## Build
##

FROM golang:1.16-buster AS build
WORKDIR /app
ENV GOPROXY=https://goproxy.cn,direct
COPY . .
COPY .netrc /root/.netrc

RUN go mod download
RUN GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -o /exam main.go

##
## Deploy
##
FROM alpine

COPY --from=build /exam /bin/exam
CMD /bin/exam
