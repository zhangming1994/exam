module gitlab.com/douwan/exam

go 1.17

require (
	github.com/Timothylock/go-signin-with-apple v0.0.0-20220114155622-499fb6086c70
	github.com/ddliu/go-httpclient v0.6.9
	github.com/fatih/color v1.13.0
	github.com/go-chi/chi v1.5.4
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/sessions v1.2.1
	github.com/iancoleman/strcase v0.2.0
	github.com/jinzhu/configor v1.2.1
	github.com/jinzhu/gorm v1.9.16
	github.com/rs/xid v1.4.0
	github.com/sirupsen/logrus v1.8.1
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tideland/golib v4.24.2+incompatible // indirect
	github.com/tideland/gorest v2.15.5+incompatible // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
