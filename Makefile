SHELL = /bin/bash

build:
	@GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -o exam main.go
	@docker build -t exam .
	@rm exam

push: build
	@$(eval REV := $(shell git rev-parse head|cut -c 1-6)-$(shell date +%s))
	@docker tag exam registry.cn-hangzhou.aliyuncs.com/douwan/exam:$(REV)
	@docker push registry.cn-hangzhou.aliyuncs.com/douwan/exam:$(REV)
	@sed -i '' "s/douwan\/exam:.*/douwan\/exam:$(REV)/" $$GOPATH/src/gitlab.com/douwan/exam/dc-deploy.yml
	@scp $$GOPATH/src/gitlab.com/douwan/exam/dc-deploy.yml apps@118.178.242.114:~/exam/docker-compose.yml
	@ssh apps@118.178.242.114 "docker pull registry.cn-hangzhou.aliyuncs.com/douwan/exam:$(REV)"
	@ssh apps@118.178.242.114 "docker-compose -f exam/docker-compose.yml down"
	@ssh apps@118.178.242.114 "docker-compose -f exam/docker-compose.yml up -d"

